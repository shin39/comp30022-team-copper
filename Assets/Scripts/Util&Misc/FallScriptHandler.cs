﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Fall script handler.
/// </summary>
public class FallScriptHandler : MonoBehaviour {


	void Update () {
		if (this.transform.position.y < -20) {
			if (this.tag.Equals ("Player") || this.tag.Equals("Enemy")) {
				this.GetComponent<AttributeHandler> ().TakeDamage (this.GetComponent<AttributeHandler> ().health, this.GetComponent<NetworkPlayer>());

			} else {
				GameObject.Destroy (this.gameObject);
			}
		}

	}
}
