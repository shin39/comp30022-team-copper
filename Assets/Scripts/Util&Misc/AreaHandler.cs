﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class AreaHandler : Photon.MonoBehaviour {

	public GameObject tilePrefab;
	public float tileStep = 7.5f;
	public int mapSize = 10;

	private int minSizeLimit = 5;

	private List<List<GameObject>> gridTiles;

	private GameObject fallenTile;
	// Use this for initialization
	void Start () {
		gridTiles = new List<List<GameObject>> ();
		GenerateField (mapSize);

		//crumbleGridTile (gridTiles);
	}
	
	// Update is called once per frame
	void Update () {
	}
    /*
    public void Crumble()
    {
        this.photonView.RPC("crumbleGridTile", PhotonTargets.All);
    }
    */
	/* Consider making this a coroutine and running through the first row
	 * Once a row is gone, the 2D grid is transposed and turned upside down
	 * we then run through the new 2D grid's first row and continue until
	 * the tile limit is reached.
	 */


    //[PunRPC]
	public void crumbleGridTile() {
		StartCoroutine_Auto (crumbling(gridTiles));
	}

	IEnumerator crumbling(List<List<GameObject>> tilesGrid) {
		int loops = 0;
		while (!minGrid (tilesGrid)) {
			tilesGrid = Reverser (tilesGrid);
			for (int i = 0; i < tilesGrid[0].Count; i++) {
				fallenTile = tilesGrid [0] [i];
				Rigidbody tile = fallenTile.GetComponent<Rigidbody> ();
				fallenTile.GetComponent<Renderer> ().material.color = Color.cyan;
				fallenTile.transform.position += Vector3.down/1000;
				fallenTile.GetComponent<MeshCollider> ().convex = true;
				tile.isKinematic = false;
				tile.useGravity = true;

				tile = null;
				fallenTile = null;
				yield return new WaitForSeconds (2f);
			}
			tilesGrid.RemoveAt(0);
			tilesGrid = Transpose (tilesGrid);
			loops++;
		}
	}


	// Transpose the tiles grid
	List<List<GameObject>> Transpose(List<List<GameObject>> tilesGrid) {

		/* Setting up the new List of List grid
		 * Since we don't assume that the grid will remain n*n,
		 * the 'row space' and 'col space' are made with that in mind
		 */
		int longestRow = tilesGrid[tilesGrid.Count-1].Count;
		//longestRow = findLongestRow(tilesGrid);

		// Setting up the new Row size based on the Col size
		List<List<GameObject>> nextTilesGrid = new List<List<GameObject>> (longestRow);
		// Setting up the new Col sizes based on the Row size
		for (int i = 0; i < longestRow; i++) {
			nextTilesGrid.Add (new List<GameObject> (tilesGrid.Count));
		}
			
		for (int col = 0; col < tilesGrid.Count; col++) {
			for (int row = 0; row < longestRow; row++) {
				// To  : the new tiles list on this row
				// Add : the tile that was in the original tilesGrid at the transpose position
				//     : any 'blank' spaces are replaced with default(GameObject)
				nextTilesGrid[row].Add(tilesGrid[col].Count > row ? tilesGrid[col][row] : default(GameObject));
			}
		}

		return nextTilesGrid;
	}

	List<List<GameObject>> Reverser (List<List<GameObject>> tilesGrid) {
		List<List<GameObject>> tempGrid = new List<List<GameObject>> ();
		for (int i = tilesGrid.Count; i > 0; i--) {
			tempGrid.Add (tilesGrid [i-1]);
		}
		return tempGrid;
	}
		
	// Use this function if transposition target can have different row sizes
	int findLongestRow(List<List<GameObject>> tilesGrid) {
		int longest = 0;

		for (int i = 0; i < tilesGrid.Count; i++) {
			longest = tilesGrid [i].Count > longest ? tilesGrid [i].Count : longest;
		}

		return longest;
	}

	bool minGrid(List<List<GameObject>> tilesGrid) {
		if (tilesGrid.Count <= minSizeLimit && tilesGrid [0].Count <= minSizeLimit) {
			return true;
		}
		return false;
	}

	/// <summary>
	/// Generates the field using square tiles.
	/// </summary>
	/// <param name="selectedSize">Field size is determined by mapSize*mapSize</param>

	void GenerateField(int selectedSize) {
	 	
		// Limits the mapSize to a minimumSizeLimit
		int mapSize = selectedSize >= minSizeLimit ? selectedSize : minSizeLimit;
		// Finds the starting x and y position based on how many tiles need to be generated
		float xStart = -(mapSize-1) * tileStep;
		float yStart = -(mapSize-1) * tileStep;

		List<GameObject> tempList;

		for (int row = 0; row < mapSize; row++) {
			// Reset the y starting position for a new col of tiles
			yStart = -(mapSize-1) * tileStep;

			tempList = new List<GameObject>();

			for (int col = 0; col < mapSize; col++) {
				GameObject newTile = Instantiate (tilePrefab);
				newTile.transform.SetParent(this.transform);
				/* Vector3 is represented as (x,y,z)
				 * We are creating 'tiles' in a x*y grid but we must translate
				 * this into Unity's axis representation: x = Vector3.x, y = Vector3.z
				 */
				newTile.transform.position = new Vector3 (xStart, 0f, yStart);

				tempList.Add (newTile);

				// move in the z axis to where next tile must be placed
				yStart += 2*tileStep;
			}
			// move in the x axis to where next tile column must be placed
			xStart += 2*tileStep;

			gridTiles.Add (tempList);

		}
	}
}
