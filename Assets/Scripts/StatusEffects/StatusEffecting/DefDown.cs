using UnityEngine;
using System.Collections;

/// <summary>
/// Defense down status effect
/// </summary>
public class DefDown : StatusEffect, IStatusEffect
{
  // defense down rate
  private float defDownRate;

  // takes defense down rate and duration
  public DefDown(float downRate, float duration)
  {
    this.defDownRate = downRate;
    this.duration    = 5f;
    this.type        = statusEffectType.Debuff;
  }

  // apply the status effect
  override public void Apply (NetworkPlayer player)
  {
    player.attributeHandler.defenceModifier /= defDownRate;
  }

  // remove the status effect
  override public void Remove (NetworkPlayer player)
  {
    player.attributeHandler.defenceModifier *= defDownRate;
  }
}
