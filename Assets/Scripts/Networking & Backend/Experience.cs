﻿using UnityEngine;
using System.Collections;
using System;

public class Experience : MonoBehaviour {

    public static Experience Instance; 

    // values retrieved from web
    private string webData;
    private string[] webStrings;

    void Start()
    {
        Instance = this;
    }

    public void AddExp(string playerName, int exp)
    {
        WWWForm www = new WWWForm();
        www.AddField("username", playerName);
        WWW w = new WWW("http://115.146.87.244/addexp.php", www);
        StartCoroutine(FindUser(w));
    }

    IEnumerator FindUser(WWW w)
    {
        yield return w;

        // web successfully returned something
        if (w.error == null)
        {
            webData = w.text;
            webStrings = webData.Split(';');

            if (webStrings[0] == "true")
            {
                Debug.Log("SUCCESSFUL");
            }
            else
            {
                Debug.Log(webStrings[1]);
            }
        }

        else
        {
            Debug.Log(w.error);
        }
    }
}
