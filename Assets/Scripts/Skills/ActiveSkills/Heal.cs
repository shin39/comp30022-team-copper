﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Blink.
/// </summary>
public class Heal : ActiveSkill, IActiveSkills
{

    #region Variables
    //public const long skillID = 857394756L;

    private const float skillCoolDown = 8f;
    private const float baseHeal = 10f;

    #endregion
    #region Public Methods

    public Heal(int level) : base()
    {
        this.level = level;
        this.coolDown = skillCoolDown;
        this.type = activeSkillType.Support;
    }


    /// <summary>
    /// Activate the skill Blink. This teleports the player to a position in front.
    ///  The player is the target.
    /// </summary>
    /// <param name="target">Target.</param>
    override public void Activate(NetworkPlayer player)
    {
        /*
        if(level >= 3)
        {
            // heal the party of the player

        }
        */
        //else
        //{
            // heal only the player
        float healed = level * baseHeal;
        player.attributeHandler.Heal((int)healed);
        //}

        player.skillHandler.startCoolDown (this);
    }


    /// <summary>
    /// Targets the enemy. Takes in the position of the character.
    /// </summary>
    /// <returns>The enemy.</returns>
    /// <param name="position">Position.</param>
    override public GameObject TargetEnemy(Vector3 position)
    {
        return null;
    }

    #endregion
}
