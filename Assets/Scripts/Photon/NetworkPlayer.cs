﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityStandardAssets.CrossPlatformInput;

/// <Summary>
/// NetworkPlayer, the script for assigning the player to camera and movement
/// script
/// </Summary>
public class NetworkPlayer : Photon.PunBehaviour, IPunObservable
{
    // denotes the player type
    public enum playerType {
      small,
      medium,
      large
    };

    // denotes the team the player is on
    public enum playerTeam {
      Red,
      Blue,
	  Yellow,
	  Green
    };

    // manages player attacking
    public AttackHandler 	     attackHandler { get; set; }
    // manages player skill
  	public SkillHandler 	     skillHandler { get; set; }
    // manages player interaction with items
    public ItemHandler 		     itemHandler { get; set; }
    // manages status effect
  	public StatusEffectHandler statusHandler { get; set; }
    // manages attribute modfying operation
  	public AttributeHandler    attributeHandler { get; set; }
    // manages key input listener
    public PlayerController    playerController { get; set; }
    // the camera script that follows the player
    public CameraWork          cameraWork { get; set; }



    public GameObject bulletPrefab;
    public String prefBulletPath;
    public int bulletSpeed = 40;

    public playerType pType;
    public playerTeam pTeam;
    public string playerName;

    private bool reloading = false;
    public bool isDead = false;


    // Update is called once per frame
    void Start()
    {
        // enables the components
        enableComponents();

        // assign the skill

        enableSkill(new Blink(2), new Heal(1));
        playerName = PlayerProperty.PLAYERNAME;


    }

    private void enableSkill(ActiveSkill skill1, ActiveSkill skill2)
    {
      if (photonView.isMine)
      {
        CharacterDecorator.assignFirstSkill(skill1, this);
        CharacterDecorator.assignSecondSkill(skill2, this);
      }
    }

    // enables and add the components that needed by a player to function
    private void enableComponents()
    {
        // find the components attached on the prefabs
        attackHandler    = GetComponent<AttackHandler>();
        itemHandler      = GetComponent<ItemHandler>();
        skillHandler     = GetComponent<SkillHandler>();
        statusHandler    = GetComponent<StatusEffectHandler> ();
        playerController = GetComponent<PlayerController>();
        cameraWork       = GetComponent<CameraWork>();
        attributeHandler = GetComponent<AttributeHandler>();

        // enables the component
        attackHandler.enabled    = true;
        itemHandler.enabled      = true;
        skillHandler.enabled     = true;
        statusHandler.enabled    = true;
        attributeHandler.enabled = true;

        // enables the component that only by the local player
        if (photonView.isMine)
        {
          cameraWork.enabled       = true;
          playerController.enabled = true;
          // attach the player to the health bar
          FindObjectOfType<Slider>().GetComponent<HealthBarBehavior>().AssignPlayer(attributeHandler);
        }

        // initialize the attribute
        attributeHandler.InitializeAttributes(pType);

    }


    // fixed update to do movement update
    void FixedUpdate()
    {
      // make sure we only move a character that belong to us
      if (!photonView.isMine || !attributeHandler.canMove)
      {

          return;
      }
      else
      {
        // get the input for movement
        float horz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        // get the speed value from attributeHandler
		float spd = attributeHandler.forwardSpeed;

	      // move the player
		playerController.Move(horz, vert, spd);

		playerController.TouchMove(CrossPlatformInputManager.GetAxis ("LHorizontal"),CrossPlatformInputManager.GetAxis ("LVertical"),spd);
		playerController.TouchLook (CrossPlatformInputManager.GetAxis ("RHorizontal"), CrossPlatformInputManager.GetAxis ("RVertical"),spd);

	  }
    }


    void Update()
    {
      if (!photonView.isMine)
      {
          return;
      }

      RouteInput(playerController.actionKeyListener());
    }


    public void CastSk1()
    {
      this.skillHandler.CastSkill(SkillHandler.SkillOrd.first, this);
    }

    public void CastSk2()
    {
      this.skillHandler.CastSkill(SkillHandler.SkillOrd.second, this);
    }

    // route the input based on the output of actionKeyListener and do the
    // action
    public void RouteInput (PlayerController.actionType output)
    {

  		switch (output) {
    		case PlayerController.actionType.Basic:
          //Debug.Log("Attempt to hurt self");
    			attributeHandler.TakeDamage (20, this);

    			//this.attackHandler.Attack ();
    			break;

    		case PlayerController.actionType.FirstSkill:
    			this.skillHandler.CastSkill(SkillHandler.SkillOrd.first, this);
    			break;

    		case PlayerController.actionType.SecondSkill:
    			this.skillHandler.CastSkill(SkillHandler.SkillOrd.second, this);
    			break;

    		case PlayerController.actionType.Item:
          if (!(this.itemHandler.EquipSlot.transform.childCount == 0))
          {
             //Fiyah();
             this.photonView.RPC("Fiyah", PhotonTargets.All);
          }
    			break;

    		case PlayerController.actionType.PickUpitem:


           this.photonView.RPC("RPCPickup", PhotonTargets.All);
    			//this.itemHandler.PickUpItem ();
    			break;

    		default:
    			break;
  	  }
    }

    [PunRPC]
    void RPCPickup ()
    {
      this.itemHandler.PickUpItem();
    }

    // starts the coroutine that applies a certain status effect to the player
    public void ApplyStatus (StatusEffect statusEffect) {
      StartCoroutine(this.statusHandler.applyEffect (statusEffect, this));
    }


    // still under development
    void OnTriggerEnter(Collider other)
    {
        if (!photonView.isMine)
        {
            return;
        }

        // We are only interested in Beamers
        // we should be using tags but for the sake of distribution, let's simply check by name.
        if (other.tag != "Bullet")
        {
            return;
        }

        // will be removed later
        attributeHandler.TakeDamage(1, this);
    }


    // parts that sync the data of this client player to others
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        // health synchronization

        if (stream.isWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(this.attributeHandler.health);
            stream.SendNext(this.isDead);

        }
        else
        {
            // Network player, receive data
            //this.health = (float)stream.ReceiveNext();
            this.attributeHandler.health = (int) stream.ReceiveNext();
            this.isDead = (bool)stream.ReceiveNext();

        }
    }

    [PunRPC]
    // fire a bullet
    public void Fiyah()
    {

        if (reloading) {
          return;
        }

        StartCoroutine_Auto(shootBullet(this.itemHandler.EquipSlot.GetComponentInChildren<RangedBulletAttack>().delay));
    }

    IEnumerator shootBullet(float delay)
    {


      var bullet = (GameObject) Instantiate (
                   bulletPrefab,
                   this.gameObject.transform.position + transform.forward * 5,
                   this.gameObject.transform.rotation
      );
      //*/
      // Add velocity to the bullet

      bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * bulletSpeed;
      reloading = true;
      yield return new WaitForSeconds(delay);
      reloading = false;

    }




	public void Death()
	{
        isDead = true;
		if (photonView.isMine)
		{

			attackHandler.enabled    = false;
			itemHandler.enabled      = false;
			skillHandler.enabled     = false;
			statusHandler.enabled    = false;
			attributeHandler.enabled = false;

			cameraWork.enabled       = false;
			playerController.enabled = false;

            ReplayManager.Instance.StopRecording();
            ReplayManager.Instance.playbackButton.SetActive(true);
			FindObjectOfType<Camera> ().GetComponent<CameraFollow> ().Reset ();
		}
		Destroy (this.gameObject, 1f);
	}
}
