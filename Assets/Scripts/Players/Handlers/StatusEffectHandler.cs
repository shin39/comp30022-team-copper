﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// manages player interaction with status effect
/// </summary>
public class StatusEffectHandler : MonoBehaviour
{
  // list of active status effect on the player
	List<StatusEffect> statusEffects { get; set; }


	// Use this for initialization
	void Start ()
  {
		statusEffects = new List<StatusEffect> ();
	}

	// Update is called once per frame
	void Update ()
  {

	}

  // coroutine to apply the effect and time the activation time.
  // when the activation times end, the status effect is removed
	public IEnumerator applyEffect (StatusEffect statusEffect, NetworkPlayer player)
  {
		//statusEffects.Add (statusEffect);
		statusEffect.Apply (player);
		//int index = statusEffects.FindIndex (statusEffect);

		yield return new WaitForSeconds (statusEffect.duration);

		statusEffect.Remove (player);
		//statusEffects.Remove (statusEffect);

	}


}
