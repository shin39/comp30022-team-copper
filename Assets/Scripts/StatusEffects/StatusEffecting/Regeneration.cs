using UnityEngine;
using System.Collections;

/// <summary>
/// Regeneration status effect, applied by ReGrowth
/// </summary>
public class Regeneration : StatusEffect, IStatusEffect
{
  private float regenFactor;

  public Regeneration (float regenFactor, float duration)
  {
    this.regenFactor = regenFactor;
    this.duration    = duration;
    this.type        = statusEffectType.Buff;
  }

  // apply the status effect
  override public void Apply (NetworkPlayer player)
  {
    //player.attributeHandler.Heal((int)regenFactor);
	  player.attributeHandler.StartHealthChange((int)regenFactor, duration);
    player.attributeHandler.isTargetable = false;
    player.attributeHandler.canMove      = false;
  }

  override public void Remove (NetworkPlayer player)
  {
    player.attributeHandler.isTargetable = true;
    player.attributeHandler.canMove      = true;
  }
}
