﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;


public class ItemSpawner : Photon.PunBehaviour {

	public List<string> itemPrefabList;

	public List<Transform> itemLocations;

	//private List<GameObject> existItems;

	private int randomTimeMin = 30;
	private int randomTimeMax = 40;

	private float timer;

	//private List<bool> itemExists = new List<bool>();

	void Start () {
		//existItems = new List<GameObject> (itemLocations.Count);
		if (!PhotonNetwork.isMasterClient)
		{
			return;
		}
		spawnItemAt (Random.Range(0,itemPrefabList.Count), Random.Range(0,itemLocations.Count));
		timer = UnityEngine.Random.Range (randomTimeMin, randomTimeMax);
		generateItem (timer);
		
	}

	// Update is called once per frame
	void Update () {

	}

	void generateItem (float timing) {
		StartCoroutine_Auto (spawnItem (timing));
	}

	IEnumerator spawnItem (float timing) {
		while (true) {
			int prefabIndex = UnityEngine.Random.Range (0, itemPrefabList.Count);

			int spawnIndex = UnityEngine.Random.Range (0, itemLocations.Count);

			string prefabLocation = itemPrefabList [prefabIndex];
			Transform spawnPosition = itemLocations [spawnIndex];

			PhotonNetwork.Instantiate (prefabLocation, spawnPosition.position, spawnPosition.rotation, 0);
			yield return new WaitForSeconds (timing);
		}
	}
	void spawnItemAt (int prefabIndex, int spawnIndex) {
		
		string prefabLocation = itemPrefabList [prefabIndex];
		Transform spawnPosition = itemLocations [spawnIndex];

		//existItems[spawnIndex] = 
		PhotonNetwork.Instantiate (prefabLocation, spawnPosition.position, spawnPosition.rotation, 0);

	}

	/*
	void spawnItems (int numItems) {
		for (int i = 0; i < numItems; i++)
		{
			int prefabIndex = UnityEngine.Random.Range(0,itemPrefabList.Count);
			
			var spawnPosition = new Vector3 (
				Random.Range(-baseSpawnCoord, baseSpawnCoord),
				2.0f,
				Random.Range(-baseSpawnCoord, baseSpawnCoord));

			var spawnRotation = Quaternion.Euler (
							20.0f, 
							Random.Range (0, 180), 
							0.0f);

			//PhotonNetwork.Instantiate ();
			//var item = (GameObject) PhotonNetwork.Instantiate ((GameObject)itemPrefabList[prefabIndex], spawnPosition);
		}
	}
	*/
}