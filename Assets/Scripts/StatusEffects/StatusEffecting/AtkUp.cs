using UnityEngine;
using System.Collections;

/// <summary>
/// Attack up status effect
/// </summary>
public class AtkUp : StatusEffect, IStatusEffect
{
  // the attack up rate
  private float atkUpRate;

  // takes the attack up rate and duration
  public AtkUp(float atkRate, float duration)
  {
    this.atkUpRate = atkRate;
    this.duration  = 5f;
    this.type      = statusEffectType.Buff;
  }

  // apply the status effect
  override public void Apply (NetworkPlayer player)
  {
    player.attributeHandler.attackModifier *= atkUpRate;
  }

  // remove the status effect
  override public void Remove (NetworkPlayer player)
  {
    player.attributeHandler.attackModifier /= atkUpRate;
  }
}
