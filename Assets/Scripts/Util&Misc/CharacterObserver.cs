﻿using UnityEngine;
using System.Collections;

public class CharacterObserver {

	Item item = null;

	// Update is called once per frame
	public bool Update (ItemHandler itemHandler) {
		if (itemHandler.currentItem != item) {
			item = itemHandler.currentItem;
			return true;
		}
		return false;
	}



}
