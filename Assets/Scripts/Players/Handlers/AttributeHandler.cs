﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// script that handles player attribute/status points changes and
/// initialization
/// </summary>
public class AttributeHandler : MonoBehaviour
{

  // health value for different player type
	private enum healthValues : int {

		smallPlayer  = 50,
		mediumPlayer = 100,
		largePlayer  = 150

	};

  // factor for regeneration calculation
  private const int REGEN_FACTOR    = 2;
  // the timeout for health increas in regeneration
  private const float REGEN_TIMEOUT = 0.5f;

  private bool isDead         {get; set;}
  private bool destroyOnDeath {get; set;}

  // modifier value that affect player attack damage
	public float attackModifier  { get; set; }
  // modifier value that affect player defense
	public float defenceModifier { get; set; }
  // modifier value that affect player speed
	public float speedModifier   { get; set; }

  // the base speed value for player, will be different for each player
	public float baseSpeed    { get; set; }
  // the forward speed value for the player
	public float forwardSpeed { get; set; }

  // max health value for the player
  // each player type have different maxhealth value
	public int maxHealth { get; set; }
  // the current health value for the player
	public int health    { get; set; }

  // basic damage value for the player
	public int basicDamage { get; set; }

  // flag that indicates if the player is currently targetable or not
  // non targetable player will not recieve any damage
	public bool isTargetable { get; set; }
  // flag  that indicates if the player is currently movable or not
	public bool canMove      { get; set; }



	void Start () {
		StartCoroutine_Auto (UpdateAttributes ());
	}


	void Update ()
  {

	}

  // updates the player attribute
	IEnumerator UpdateAttributes ()
  {
		while (true)
    {
			forwardSpeed = baseSpeed * speedModifier;
			//forwardSpeed *= speed;
			yield return new WaitForSeconds (0.1f);
		}
	}


  // initialize the attribute for a player based on the player type
	public void InitializeAttributes (NetworkPlayer.playerType player)
  {
    // common attribute for all player
    isTargetable = true;
    canMove      = true;
    isDead       = false;
    destroyOnDeath = true;

		switch (player) {

  		case NetworkPlayer.playerType.small:
  			attackModifier  = 1f;
  			defenceModifier = 1f;
  			speedModifier   = 1f;

  			baseSpeed    = 2f;
  			forwardSpeed = 2f;

  			maxHealth = (int)healthValues.smallPlayer;
  			health    = maxHealth;

  			basicDamage  = 5;
  			break;

  		case NetworkPlayer.playerType.medium:
  			attackModifier  = 1f;
  			defenceModifier = 1f;
  			speedModifier   = 1f;

  			baseSpeed    = 1.5f;
  			forwardSpeed = 1.5f;

  			maxHealth   = (int)healthValues.mediumPlayer;
  			health      = maxHealth;

  			basicDamage  = 10;
  			break;

  		case NetworkPlayer.playerType.large:
  			attackModifier  = 1f;
  			defenceModifier = 1f;
  			speedModifier   = 1f;

  			baseSpeed    = 1f;
  			forwardSpeed = 1f;

  			maxHealth = (int)healthValues.largePlayer;
  			health    = maxHealth;

  			basicDamage  = 20;
  			break;
  		}

	}


  /// <summary>
  /// increase/heal the player health with the healAmount.
  /// will not heal player too much
  /// </summary>
  public void Heal (int healAmount)
  {
		if ((health + healAmount) > maxHealth )
    {
			health = maxHealth;
		}
		else
    {
			health += healAmount;
		}

    return;
	}


  // starts the coroutine to do incremental health change over time
  public void StartHealthChange (int val, float duration)
  {
		StartCoroutine_Auto (TimedHealthChange (val, duration));
	}


	/// <summary>
	/// Changes the health over time per second.
	/// </summary>
	/// <returns>The health change.</returns>
	/// <param name="value">Value.</param>
	public IEnumerator TimedHealthChange (int val, float duration)
  {
		float dur = duration;

		while (dur > 0)
    {
			Heal(val / REGEN_FACTOR);

			dur -= REGEN_TIMEOUT;

			yield return new WaitForSeconds (0.5f);
	  }

  }




	/// <summary>
	/// Takes the damage.
	/// </summary>
	/// <returns><c>true</c>, if damage was taken, <c>false</c> otherwise.</returns>
	/// <param name="amount">Amount.</param>
	/// <param name="player">Player.</param>

	public bool TakeDamage(int amount, NetworkPlayer player)
	{
		bool isDead = false;
		if (!isTargetable) {
			return false;
		}
		health -= amount;
		if (health <= 0)
		{
			if (destroyOnDeath)
			{
				isDead = true;
				GetComponent<NetworkPlayer> ().Death ();
			}
			else
			{
				health = maxHealth;
			}
		}
		return isDead;
	}


}
