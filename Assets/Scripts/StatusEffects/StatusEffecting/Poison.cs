using UnityEngine;
using System.Collections;

/// <summary>
/// poison status effect when the player is poisoned
/// </summary>
public class Poison: StatusEffect, IStatusEffect
{
  // the damage per tick for poison
  private float poisonDmg;

  public Poison(float poisonDmg, float duration)
  {
    // minus the value to make it into damage
    this.poisonDmg = -poisonDmg;
    this.duration  = 5f;
    this.type      = statusEffectType.Debuff;
  }

  // apply the status effect
  override public void Apply (NetworkPlayer player)
  {
    //player.attributeHandler.StartHealthChange((int)poisonDmg, duration);
  }

  // remove the status effect
  override public void Remove (NetworkPlayer player)
  {

  }
}
