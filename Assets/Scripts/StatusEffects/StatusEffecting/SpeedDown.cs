using UnityEngine;
using System.Collections;

/// <summary>
/// Speed down status effect
/// </summary>
public class SpeedDown : StatusEffect, IStatusEffect
{
  // speed down rate
  private float speedDownRate;

  // takes speed down rate and duration
  public SpeedDown(float downRate, float duration)
  {
    this.speedDownRate = downRate;
    this.duration      = 5f;
    this.type          = statusEffectType.Debuff;
  }

  // apply the status effect
  override public void Apply (NetworkPlayer player)
  {
    player.attributeHandler.speedModifier /= speedDownRate;
  }

  // remove the status effect
  override public void Remove (NetworkPlayer player)
  {
    player.attributeHandler.speedModifier *= speedDownRate;
  }
}
