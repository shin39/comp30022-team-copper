﻿using UnityEngine;
using System.Collections;

public class PistolBullet : Photon.PunBehaviour
{

  public int damage = 10;
  void Start ()
  {
      Destroy(this.gameObject,7f);
  }

    void OnCollisionEnter(Collision collision)
    {
        var hit = collision.gameObject;
        var health = hit.GetComponent<AttributeHandler>();
        //Debug.Log(collision.gameObject.name);
        if (health != null)
        {
	        health.TakeDamage(10, hit.GetComponent<NetworkPlayer>());
			//if (photonView.isMine){
			  ///  PhotonNetwork.Destroy(this.gameObject);
			//}
			Destroy(gameObject);
        }


    }
}
