using UnityEngine;
using System.Collections;

/// <summary>
/// Attack down status effect
/// </summary>
public class AtkDown : StatusEffect, IStatusEffect
{
  // attack down rate
  private float atkDownRate;

  // takes attack down rate and duration
  public AtkDown(float downRate, float duration)
  {
    this.atkDownRate = downRate;
    this.duration    = 5f;
    this.type        = statusEffectType.Debuff;
  }

  // apply the status effect
  override public void Apply (NetworkPlayer player)
  {
    player.attributeHandler.attackModifier /= atkDownRate;
  }

  // remove the status effect
  override public void Remove (NetworkPlayer player)
  {
    player.attributeHandler.attackModifier *= atkDownRate;
  }
}
