﻿using UnityEngine;
using System.Collections;

public class ReplayManager : MonoBehaviour {

    // singleton
    public static ReplayManager Instance;
    public GameObject playbackButton;

    void Start()
    {
        Instance = this;
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void StartRecording()
    {
        Everyplay.StartRecording();
    }

    public void StopRecording()
    {
        Everyplay.StopRecording();
    }

    public void PlayLastRecording()
    {
        Everyplay.PlayLastRecording();
    }
}
