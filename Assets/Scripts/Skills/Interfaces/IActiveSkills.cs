﻿using UnityEngine;
using System.Collections;



public interface IActiveSkills : ISkills {



	void Activate (NetworkPlayer player);

	GameObject TargetEnemy (Vector3 position);


}
