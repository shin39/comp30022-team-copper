﻿using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Blink.
/// </summary>
public class Swap : ActiveSkill, IActiveSkills
{

    #region Variables
    //public const long skillID = 857394756L;

    private const float skillCoolDown = 2f;
    private const float baseSpeed = 30f;

	//private GameObject swapBulletPrefab;
	private const string bulletPath = "Prefabs/Weapons/Misc/SwapBullet";
    #endregion
    #region Public Methods

    public Swap(int level) : base()
    {
		//swapBulletPrefab = Resources.Load ("Prefabs/Weapons/Misc/SwapBullet") as GameObject;
        this.level = level;
        this.coolDown = skillCoolDown;
        this.type = activeSkillType.Utility;
    }


    /// <summary>
    /// Activate the skill Blink. This teleports the player to a position in front.
    ///  The player is the target.
    /// </summary>
    /// <param name="target">Target.</param>
    override public void Activate(NetworkPlayer player)
    {
		//SwapBullet swapBullet = (SwapBullet)Instantiate(swapBulletPrefab, new Vector3(player.transform.position.x +
        //    0.5f, 1f, player.transform.position.z + 0.5f), player.transform.rotation);
		//player.createBullet ( typeof(SwapBullet), bulletPath);
		//swapBullet.Start ();
		//swapBullet.setSpeed (player.transform, baseSpeed * level);
		//swapBullet.caster = player;
		//Debug.Log (swapBullet.caster.tag);
		//swapBullet.caster = player;


		UnityEngine.Object prefab = Resources.Load (bulletPath);
		Vector3 direction = player.transform.forward;

		GameObject bullet = (GameObject)GameObject.Instantiate(prefab, new Vector3(player.transform.position.x
			, 2f, player.transform.position.z) + player.transform.forward*2, player.transform.rotation);
		bullet.AddComponent<SwapBullet> ();
		SwapBullet swapScript = bullet.GetComponent<SwapBullet> ();
		swapScript.caster = player;
		swapScript.setSpeed (player.transform, baseSpeed*level);

    }

    public static void SwapPlayers(Transform p1, Transform p2)
    {
        Vector3 temp = p1.position;
        p1.position = p2.position;
        p2.position = temp;
    }


    /// <summary>
    /// Targets the enemy. Takes in the position of the character.
    /// </summary>
    /// <returns>The enemy.</returns>
    /// <param name="position">Position.</param>
    override public GameObject TargetEnemy(Vector3 position)
    {
        return null;
    }

    #endregion
}
