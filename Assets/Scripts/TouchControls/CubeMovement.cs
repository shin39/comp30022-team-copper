﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class CubeMovement : MonoBehaviour
{
	public float moveSpeed = 0.05f;
	public float rotation = 2f;

	void Start ()
	{
		
	}

	void Update ()
	{
		Vector3 moveVec = new Vector3(CrossPlatformInputManager.GetAxis("LHorizontal"),CrossPlatformInputManager.GetAxis("LVertical")) * moveSpeed;
		this.transform.position += moveVec;
		//this.transform.LookAt(new Vector3 (CrossPlatformInputManager.GetAxis ("RHorizontal"), CrossPlatformInputManager.GetAxis ("RVertical")),Vector3.back);
		if (CrossPlatformInputManager.GetAxis("RVertical") != 0) {
			
			float zAngle = CrossPlatformInputManager.GetAxis("RHorizontal") / CrossPlatformInputManager.GetAxis("RVertical");
			float degree = Mathf.Rad2Deg * Mathf.Atan (zAngle);
			this.transform.rotation = Quaternion.Euler (Vector3.back * degree);
		}
		//this.transform.rotation = Quaternion.Euler (Vector3.back * -30.05f);
	}
}
