﻿using UnityEngine;
using System.Collections;

public class ShieldEffect : StatusEffect, IStatusEffect{


	private const string shieldPath = "Prefabs/CosmeticEffects/Sphere";
	UnityEngine.Object prefab;
	private GameObject shield;


	public ShieldEffect (float duration) {
		this.duration = duration;
		this.type = statusEffectType.Cosmetic;
		this.prefab  = Resources.Load (shieldPath);
		this.shield = (GameObject)GameObject.Instantiate (prefab);
	}

	override public void Apply (NetworkPlayer player) {

		this.shield.transform.parent = player.transform;
		this.shield.transform.position = player.transform.position;
			//new Vector3(player.transform.position.x, 2, player.transform.position.z);
	}

	override public void Remove (NetworkPlayer player) {
		GameObject.Destroy (shield);
	}

}
