﻿using UnityEngine;
using System.Collections;

public interface IStatusEffect {

	// Use this for initialization
	void Apply (NetworkPlayer player);
	void Remove (NetworkPlayer player);

}
