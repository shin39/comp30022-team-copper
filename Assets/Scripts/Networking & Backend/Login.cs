﻿/*
 * This class reads user input to username and password field and 
 * attempt to search for the user in our database
 * proceed to the next scene if the user exists in our database
 * written by : Erwin Haryantho <664480>
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Login : MonoBehaviour
{

    public InputField username;
    public InputField password;

    // values retrieved from web
    private string webData;
    private string[] webStrings;

    public Text status;

    // encryption 
    private XorEncyption encryption;

    void Start()
    {
        encryption = new XorEncyption();
    }

    public void SubmitInput()
    {
        // status text
        string usernameText = "username: " + username.text + "\n";
        string passText = "password: " + password.text + "\n";
        status.text = usernameText + passText;
        GetUser(username.text, password.text);

        // reactive the field
        username.text = "";
        username.ActivateInputField();
        password.text = "";
        password.ActivateInputField();
    }

    public void DisplayRegister()
    {
        SceneManage.DisplayRegister();
    }

    public void GetUser(string user, string pass)
    {
        string encryptedPass = encryption.Encrypt(pass);
        
        // access our php file
        WWWForm www = new WWWForm();
        www.AddField("username", user);
        www.AddField("password", pass);
        WWW w = new WWW("http://115.146.87.244/getuser.php", www);
        StartCoroutine(FindUser(w, user));
        
    }

    // wait for the web to yield result from db search
    IEnumerator FindUser(WWW w, string user)
    {
        yield return w;

        // web successfully returned something
        if (w.error == null)
        {
            webData = w.text;
            webStrings = webData.Split(';');

            if (webStrings[0] == "true")
            {
                // user found
                // store the username in a static object
                PlayerProperty.PLAYERNAME = user;
                SceneManage.ChangeScene();
            }
            else
            {
                status.text = webStrings[1];
            }

        }

        // error in connecting to web
        else
        {
            status.text = w.error.ToString();
        }

    }

}


