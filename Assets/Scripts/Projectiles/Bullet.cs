﻿using UnityEngine;
using System.Collections;


public abstract class Bullet : MonoBehaviour, IBullet {

	public void OnTriggerEnter (Collision collision) {}
}