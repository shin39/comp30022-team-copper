﻿using UnityEngine;
using System.Collections;

public class DataLoader : MonoBehaviour {

    public string[] users;

	// Use this for initialization
	IEnumerator Start () {
        WWW usersData = new WWW("http://localhost/gladiator/gg.php");
        // wait to download users data 
        yield return usersData;
        string usersDataString = usersData.text;
        print(usersDataString);
        users = usersDataString.Split(';');
        //print(getDataValue(users[0], "username:"));
	}

    string getDataValue(string data, string index){
        // get the value from data specified by sub-string index
        string value = data.Substring(data.IndexOf(index)+ index.Length);
        // remove everything after |
        if(value.Contains("|"))
            value = value.Remove(value.IndexOf("|"));
        return value;
    }

}
