﻿using UnityEngine;
using System.Collections;

public class SkillHandler : MonoBehaviour {

  // player's first active skill slot
	ActiveSkill firstSkill { get; set; }
  // player's second active skill slot
	ActiveSkill secondSkill { get; set; }
  // player's passive skill slot
	PassiveSkill passive { get; set; }

  // the skill code to differentiate between first, second or passive skill
	public enum SkillOrd : int
  {
		first = 1,
		second = 2,
		passive = 3
	};


	void Start ()
  {


	}

  // cast/activate the skill
	public void CastSkill(SkillOrd skillOrd, NetworkPlayer player) {

		switch (skillOrd) {
  		case SkillOrd.first:
  			if (firstSkill.onCoolDown)
  				break;
  			firstSkill.Activate (player);
  			startCoolDown (firstSkill);
  			break;

  		case SkillOrd.second:
  			if (secondSkill.onCoolDown)
  				break;
  			secondSkill.Activate (player);
  			startCoolDown (secondSkill);
  			break;

  		default:
  			break;

		}

	}

  // assign the skill into the skill handler according to its slot
	public void InitializeSkill (SkillOrd skillOrd, ISkills skill) {

		switch (skillOrd) {

			case SkillOrd.first:
				firstSkill = (ActiveSkill)skill;
				break;
			case SkillOrd.second:
				secondSkill = (ActiveSkill)skill;
				break;
			case SkillOrd.passive:
				passive = (PassiveSkill)skill;
				break;
			default:
				break;

		}

	}


  // coroutine indicating skill cooldown
	IEnumerator CoolDown(ActiveSkill skill) {

		skill.onCoolDown = true;
		yield return new WaitForSeconds (skill.coolDown);
		skill.onCoolDown = false;

	}


  // coroutine to start skill CoolDown
  // when a skill is on cooldown, it cannot be used
	public void startCoolDown (ActiveSkill skill) {
		StartCoroutine_Auto (CoolDown(skill));
	}

}
