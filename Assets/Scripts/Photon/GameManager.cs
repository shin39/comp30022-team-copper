using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameManager : Photon.PunBehaviour
{

    // singleton
    static public GameManager Instance;

    private bool hasCharacter = false;
    private bool startGame = false;
    private int playerConnected = 0;
    private int playerComponentsNum = 4;

    public GameObject[] fallable;
	public bool winnerExists = false;
    private string winnerName = "";
	public int roundTime;

	public GameObject itemSpawningHandler;
	public GameObject arenaDropHandler;
    public Text endGameText;
    private int spawnLocations;
    public List<Transform> spawnPoints;
    public GUISkin skin;


    void Start()
    {
        Instance = this;
        List<Transform> spawnPoints = new List<Transform>(spawnLocations);
        StartCoroutine_Auto (this.checkFall());

    }

    void Update()
    {
		//Start game timer: call crumbleGridTile (gridTiles);
        if(!startGame && PhotonNetwork.room != null)
        {
            gameObject.GetComponent<MenuManager>().SetStatusText(PhotonNetwork.room.playerCount, PhotonNetwork.room.maxPlayers);
            if(PhotonNetwork.room.playerCount == PhotonNetwork.room.maxPlayers)
            {
				BeginRound ();
            }
        }

        if (winnerExists)
        {
            EndRound();
        }
    }

    private void EndRound()
    {
        Debug.Log("GAMEOVER");
        winnerExists = false;
        endGameText.text = winnerName + " WON";
        Experience.Instance.AddExp(winnerName, 500);
    }

    public void OnGUI()
    {
        if (this.skin != null)
        {
            GUI.skin = this.skin;
        }

        // only allow player to spawn character when they havent choosen
        // change the prefab and spawn position
        if (!hasCharacter && PhotonNetwork.connected)
        {
            if (PhotonNetwork.room != null && startGame)
            {
                GUILayout.Space(40);
                if (GUILayout.Button("Small Guy"))
                {
                    PhotonNetwork.Instantiate("Prefabs/Characters/SmallSlime", GetRandomPoint(), Quaternion.identity, 0);
                    hasCharacter = true;
                }
                GUILayout.Space(20);
                if (GUILayout.Button("Medium Guy"))
                {
                    PhotonNetwork.Instantiate("Prefabs/Characters/MediumSlime", GetRandomPoint(), Quaternion.identity, 0);
                    hasCharacter = true;
                }
                GUILayout.Space(20);
                if (GUILayout.Button("Big Guy"))
                {
                    PhotonNetwork.Instantiate("Prefabs/Characters/BigSlime", GetRandomPoint(), Quaternion.identity, 0);
                    hasCharacter = true;
                }
            }
        }
    }

    private Vector3 GetRandomPoint()
    {
        int randomIndex = UnityEngine.Random.Range(0, spawnPoints.Count);
        Transform randomPoint = spawnPoints[randomIndex];
        spawnPoints.RemoveAt(randomIndex);
        spawnPoints.TrimExcess();
        return randomPoint.position;
    }

    /*void Update()
    {
        if(PhotonNetwork.room.playerCount < PhotonNetwork.room.maxPlayers)
        {
            // pause the scene
        }
        else
        {
            // wait approx 5 s before starting, give messages to player
            // start the scene
        }
    }*/

    public override void OnPhotonPlayerConnected(PhotonPlayer other)
    {
        Debug.Log("OnPhotonPlayerConnected() " + other.name); // not seen if you're the player connecting


        if (PhotonNetwork.isMasterClient)
        {
            Debug.Log("OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected


            //LoadArena();
        }
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer other)
    {
        Debug.Log("OnPhotonPlayerDisconnected() " + other.name); // seen when other disconnects


        if (PhotonNetwork.isMasterClient)
        {
            Debug.Log("OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected

            //LoadArena();
        }
    }

    /// Called when the local player left the room. We need to load the launcher scene.
    /// </summary>
    public override void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom (local)");
        SceneManager.LoadScene("Lobby");
    }


    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

	public void BeginRound()
	{

        PhotonNetwork.room.visible = false;
        this.photonView.RPC("SetStartGame", PhotonTargets.All);
        itemSpawningHandler.SetActive (true);
		//StartCoroutine_Auto (roundTimeCoroutine (roundTime));
	}

	IEnumerator roundTimeCoroutine(int seconds)
	{
		for (int i = 0; i < seconds; i++)
		{
			yield return new WaitForSeconds (1f);
		}
		//arenaDropHandler.GetComponent<AreaHandler>().Crumble();
	}

    [PunRPC]
    void SetStartGame()
    {
        startGame = true;
        ReplayManager.Instance.StartRecording();
        arenaDropHandler.GetComponent<AreaHandler>().crumbleGridTile();
        StartCoroutine_Auto(this.checkLast());
    }

	#region EndChecker

	IEnumerator checkLast () {

        int playerCount = PhotonNetwork.room.playerCount;
		for (;;) {
            NetworkPlayer[] players = FindObjectsOfType<NetworkPlayer>();
            foreach(NetworkPlayer player in players)
            {
                if (player.isDead)
                {
                    playerCount--;
                }
            }
            if (playerCount == 1) {
				winnerExists = true;
                if (string.IsNullOrEmpty(winnerName))
                {
                    winnerName = players[0].playerName ;
                }
				break;
			}
      yield return new WaitForSeconds(0.1f);
		}

	}

	#endregion


	#region Fall Handle



	/// <summary>
	/// Checks the fall.
	/// </summary>
	/// <returns>The fall.</returns>
	IEnumerator checkFall () {
		bool killed = false;
		while (true) {
			fallable = Refresher.refreshFallable();
			killed = false;
			foreach (GameObject o in fallable) {

				if (o.transform.position.y < -20) {

					if ((o.tag.Equals ("Player") || o.tag.Equals ("Enemy")) && !killed) {
						killed = o.GetComponent<AttributeHandler> ().TakeDamage (
							o.GetComponent<AttributeHandler> ().health,
							o.GetComponent<NetworkPlayer> ());
						//killed = true;

					} else {
						GameObject.Destroy (o);
					}
				}
			}
			yield return new WaitForSeconds(0.1f);
		}
	}



	#endregion

	/// <summary>
	/// Refresher.
	/// </summary>
	class Refresher {

		/// <summary>
		/// Refreshs the fallable.
		/// </summary>
		/// <returns>The fallable.</returns>
		public static GameObject[] refreshFallable () {

			var players = GameObject.FindGameObjectsWithTag ("Player");
			var enemies = GameObject.FindGameObjectsWithTag ("Enemy");
			var tiles = GameObject.FindGameObjectsWithTag ("Tile");
			var items = GameObject.FindGameObjectsWithTag ("Item");

			GameObject[] fallable = new GameObject[players.Length +
				enemies.Length +
				tiles.Length   +
				items.Length];

			players.CopyTo (fallable, 0);
			enemies.CopyTo (fallable, players.Length);
			tiles.CopyTo (fallable, players.Length + enemies.Length);
			items.CopyTo (fallable, players.Length + enemies.Length + tiles.Length);

			return fallable;
		}

	}


}
