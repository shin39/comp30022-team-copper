using UnityEngine;
using System.Collections;


/// <summary>
/// ReGrowth skill
/// when activated makes the player unable to move for a
/// certain duration, then heal himself during that period.
/// The player is also invulnerable during that period
/// </summary>
public class ReGrowth : ActiveSkill, IActiveSkills
{

	public const long skillID = 834364856L;

	private const float skillCoolDown = 10f;
	// for upgrade each level either improve regen
	// or duration
	// regen 10 > 10.5 > 11
	// duration 2 > 2,5 > 3
	private const float baseRegen     = 15f;
	private const float baseDuration  = 2f;

	// local variables
	private float regen;
	private float duration;


	public ReGrowth (int level) : base()
  	{
		this.level = level;
		this.type  = activeSkillType.Defensive;
		this.coolDown = skillCoolDown;
	}

  // activation
	override public void Activate (NetworkPlayer player)
  	{
    regen    = baseRegen;
    duration = baseDuration;

    player.ApplyStatus (new Regeneration (regen, duration));


	}


	override public GameObject TargetEnemy (Vector3 position) {
		return null;
	}

}
