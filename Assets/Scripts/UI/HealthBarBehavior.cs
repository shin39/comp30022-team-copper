using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// Script that dictates the health bar slider behaviours
/// </summary>
public class HealthBarBehavior : MonoBehaviour
{
  // health point bar constant value
  private const int HP_BAR_CONSTANT = 100;

  // the slider that is used for the health bar
  public Slider healthBar;
  // the handler from a player to acces the health value
  private AttributeHandler playerAttb;



  void Start()
  {


  }


  // handles the update
  void LateUpdate()
  {
    // update the health bar if there is a player to monitor
    if (playerAttb != null)
    {
      healthBar.value = playerAttb.health * HP_BAR_CONSTANT / playerAttb.maxHealth;
    }
  }


  // assigns the player AttributeHandler to monitor the health change
  public void AssignPlayer(AttributeHandler attbHdlr)
  {
    this.playerAttb = attbHdlr;
    return;
  }
}
