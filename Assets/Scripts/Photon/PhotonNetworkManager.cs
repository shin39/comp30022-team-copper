﻿using UnityEngine;
using System.Collections;

public class PhotonNetworkManager : MonoBehaviour
{

    public static string VERSION = "1.00";
    public string roomName = "myroom";
    //private bool isConnecting = false;

    // Use this for initialization

    void Awake()
    {
        //PhotonNetwork.autoJoinLobby = false;

        // done in lobby
        //PhotonNetwork.automaticallySyncScene = true; 
    }

    void Start()
    {
        Connect();
        //isConnected = true;
    }

    void Connect()
    {
        Debug.Log("CONNECTING");
        if (PhotonNetwork.connected)
        {
            // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnPhotonRandomJoinFailed() and we'll create one.
            //PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            // #Critical, we must first and foremost connect to Photon Online Server.
            PhotonNetwork.ConnectUsingSettings(VERSION);
        }

    }

    /*void OnJoinedLobby()
    {
        Debug.Log("ONJOINLOBBY");
        RoomOptions roomOptions = new RoomOptions() { IsVisible = false, MaxPlayers = 4 };
        PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
    }*/

    public void Disconnect()
    {
        /*if (PhotonNetwork.connected == false)
            return;*/
        PhotonNetwork.Disconnect();
    }

    void OnJoinedRoom()
    {
        Debug.Log("ONJOINROOM");
        //PhotonNetwork.Instantiate("PlayerRed", new Vector3(0, 0, 0), Quaternion.identity, 0);
    }

   



}

