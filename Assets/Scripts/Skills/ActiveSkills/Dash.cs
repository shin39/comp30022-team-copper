﻿using UnityEngine;
using System.Collections;

public class Dash : ActiveSkill, IActiveSkills {

	#region Variables
	public const long skillID = 847364856L;

	private const float skillCoolDown = 3f;
	private const float speedUpFactor = 1.5f;
	private const float baseDuration = 1f;
	#endregion

	public Dash (int level) : base() {
		this.level = level;
		this.type = activeSkillType.Utility;
		this.coolDown = skillCoolDown;
	}


	override public void Activate (NetworkPlayer player) {
		player.ApplyStatus (new SpeedUp (speedUpFactor, 0f));

		//StartCoroutine_Auto (DashRunnable);
	}

	override public GameObject TargetEnemy (Vector3 position) {
		return null;
	}

}
