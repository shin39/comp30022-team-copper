﻿using UnityEngine;
using System.Collections;

public abstract class ActiveSkill : IActiveSkills {

	public enum activeSkillType {
		MeleeDamage,
		Ranged,
		Defensive,
		Support,
		Utility
	};

	public ActiveSkill () {
		this.onCoolDown = false;
	}


	public int level { get; set; }
	public float coolDown;

	protected activeSkillType type { get; set; }
	public bool onCoolDown;

	virtual public void Activate (NetworkPlayer player){}

	virtual public GameObject TargetEnemy (Vector3 target){return null;}

	protected void startCoolDown () {
		//MonoBehaviour _ref = new MonoBehaviour ();
		//_ref.StartCoroutine (CoolDown());
		//MonoBehaviour.print ("TeST");

	}


}
