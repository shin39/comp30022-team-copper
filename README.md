# README #


## COMP 30022 TEAM COPPER IT PROJECT ##

### Quick Summary###
This is a repository for 'Gelatin Gladiator', a game in development by Team Copper.

The game will be a multiplayer online battle arena game, which will be similar to
   
   * Crash Bandicoot tilt panic https://youtu.be/Kgd9Rd5745w
   * DoTA2 custom game, Crumbling Island Arena https://youtu.be/q_rOU-C2SyE

We aim to make the game available on Android devices



## How to run the App? ##

This project is build using Unity 5.4.x
You can run the app through unity, but you need to make sure that the build platform is Android. 
To change it, go to build settings, and then choose android as build platform.

You can also build the apk version of the file, which you can install and run on an android device.
However you will need android sdk (can be downloaded from: https://developer.android.com/studio/index.html).
This is the step to build the project
 
1.  Download the build or pull the repository if you have a clone on your machine
2.  Open unity and select the downloaded directory.
3.  Under file menu, choose build settings.
4.  For the scenes, include the login, lobby and then the main scene to the build.
5.  Choose android and click switch platform, 
6.  locate the android sdk, you will asked to locate it before you can build the apk.
7.  build the apk, place it to the destination you want.
8.  copy the apk into your android device, and install it




## Creating a game room and playing ##

### Starting the game ###
when you start the program/application, you will enter a scene where you have the option to create a game room with a specified name, join one with specified name or join random room.

### In a room ###
If you are room master (the one that created the room) you can start the game even though the room is still not full yet. If you are not a room master, you need to wait for the room master to start the game.

### Game start ###
The game will only start when there are at least 2 player.
Before the game started, players can choose from 3 different slime. They all have different traits.
* small slime have small hitbox, higher movement speed and low hp.
* medium slime have medium hitbox, medium movement speed and medium hp.
* big slime have large hitbox, slow movement speed and more hp compared to others
When the game begins, each player will be spawned at one of the 4 corners of the game arena. The player can only attack when he has a weapon. The weapons will be spawned at the center and close to the 4 corners of the stage.

## Game Rules ##
For the current version of the game, these are the rules
* player will be dead when his health hit zero or when they fall
* player can only attack when they have weapon. The weapons will be spawned around the center of the arena periodically. 
* the arena will crumble after certain period of time has passed

## Watching Replays ##
Currently, once a player has died, a button will appear, prompting the user to select if they want to view the previous replay. If they press it, they will be taken to the replay player and can view their last played game.


## Game Controls ##
### Android Controls ###
![Slide1.jpg](https://bitbucket.org/repo/xBKdyA/images/2984558654-Slide1.jpg)

above are the layout of the controller UI on android devices. 
* health bar : indicates the player's current health
* chat button : will toggle the chat dialog bar on the bottom
* left analog : to move the player
* right analog : to rotate the player view
* pickup/drop button : to pickup items
* attack button : to attack/use item
* skill buttons : there are 2 skill buttons, to use first skill and second skill
* quit button : to quit the game


### Desktop Controls ###
    * AWSD keys or arrow keys for movement
    * Space key to pickup items
    * Shift key to use 1st skill
    * Control key (cmd key on mac) to use 2nd skill
    * Z key to damage yourself > debug
    * M to attack 
    * L to rotate view left
    * ' to rotate view right
     
## Announcements ##
### 18/9/2016 - Bug ###
Item handler does not handle weapons as good as playertestweapon1 prefab did, weapon is not flying around. Will attempt to fix tomorrow on awakening.


### 17/9/2016 - Bug ###
Game handler no longer revives players who fall off more than one time. Fix in progress.


### 14/9/2016 - Bug ###
There is a player with the player prefab. When using player prefab, not playerweapontest prefab the player falls through the stage.


### 30/8/2016 - Bug ###
To be able to continue testing, the build has been swapped back to unity standalone. Keyboard input now still works. Bug with camera when player dies.


### 30/8/2016 - Touchscreen input added ###
In order to add touchscreen input, the project build has been migrated to android. As of now, it is not useable. We are currently attempting to have proper touchscreen input.


### 25/8/2016 - New Testing Methods! ###
Simple testing has become available on the Simple-Testing-改 branch. To test in unity, load the
test.unity scene and run. The testing output will be printed on the unity console.


## Team Copper Member ##
Erwin Haryantho         664480  eharyantho@student.unimelb.edu.au

Glory Wiguno            690810  gwiguno@student.unimelb.edu.au

Primaniaga Suryadi      709854  psuryadi@student.unimelb.edu.au

Ryan Chew               694818  rchew1@student.unimelb.edu.au






[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)