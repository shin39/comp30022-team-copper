﻿using UnityEngine;
using System.Collections.Generic;


public class MeleeAttack : IAttack {

	public int basicMeleeDamage { get; set; }

	public List<GameObject>nearEnemy = new List<GameObject>();
	public List<GameObject>deadEnemies = new List<GameObject>();
	Animation anim;

	// Use this for initialization
	public MeleeAttack () {
		basicMeleeDamage = 10;
	}

	public void Attack() {

		deadEnemies.Clear ();



		foreach (GameObject enemy in nearEnemy)
		{
			if (enemy.GetComponent <AttributeHandler> ().TakeDamage (basicMeleeDamage,
					enemy.GetComponent<NetworkPlayer>()))
			{
				deadEnemies.Add (enemy);
			}
		}

		foreach (GameObject enemy in deadEnemies)
		{
			nearEnemy.Remove (enemy);
		}


	}



}
