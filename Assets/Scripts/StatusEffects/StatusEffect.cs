﻿using UnityEngine;
using System.Collections;

public abstract class StatusEffect : IStatusEffect {

	public enum statusEffectType {

		Buff,
		Debuff,
		Cosmetic

	};
	public float duration { get; set; }
	protected statusEffectType type { get; set; }

	virtual public void Apply (NetworkPlayer player){}
	virtual public void Remove (NetworkPlayer player){}

}
