﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Handles the interaction and operation between player and items
/// </summmary>
public class ItemHandler : MonoBehaviour
{
  // current item the player hold
	public Item currentItem { get; set; }
  // list of items that can be picked up by the layer
	public List<GameObject> pickableItems = new List<GameObject>();
  // player's item equip slot
	public GameObject EquipSlot;

	// Use this for initialization
	void Start ()
  {
		currentItem = null;
		EquipSlot   = transform.Find ("EquippedWeapon").gameObject;
	}

	// Update is called once per frame
	void Update ()
  {
		if (EquipSlot.transform.childCount > 0) {
			EquipSlot.transform.GetChild (0).transform.position = EquipSlot.transform.position;
			EquipSlot.transform.GetChild (0).transform.rotation = EquipSlot.transform.rotation;
		}
	}


  // pickup an item if it exist and close enough for the player to get
  [PunRPC]
	public bool PickUpItem () {
		if (pickableItems.Count == 0 && EquipSlot.transform.childCount == 0) {
			return false;
		}
		if (EquipSlot.transform.childCount == 0) {
			GameObject item = pickableItems [0];
			item.transform.parent = EquipSlot.transform;
			item.transform.position = EquipSlot.transform.position;
			item.transform.rotation = EquipSlot.transform.rotation;
			item.GetComponent<Item>().isPicked = true;
			pickableItems.Remove (item);
		}
		else {
			EquipSlot.GetComponentInChildren<Item>().isPicked = false;
			EquipSlot.transform.GetChild (0).transform.parent = null;
		}
		return true;
	}



	void OnTriggerEnter (Collider col) {
		if (!col.tag.Equals("Item") || col.gameObject.GetComponent<Item>().isPicked) {
      //Debug.Log("Item entered");
      //Debug.Log(col.tag);
			return;
		}
    //Debug.Log(col.tag);
		pickableItems.Add (col.gameObject);
	}

	void OnTriggerExit (Collider col) {
		if (!col.tag.Equals("Item")) {
			return;
		}
		pickableItems.Remove (col.gameObject);
	}

}
