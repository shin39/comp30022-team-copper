﻿using UnityEngine;
using System.Collections.Generic;

public class AttackHandler : MonoBehaviour {

	MeleeAttack meleeAttack { get; set; }
	RangedBulletAttack rbWeapon { get; set; }


  // the type of attack player can do
	public enum attackTypes {
		Melee,
		RangedBullet,
		RangedRaycast
	};

  // current attack type that the player can do
	attackTypes currentAttack;

	// Use this for initialization
	void Start () {

		meleeAttack = new MeleeAttack ();
    //gameObject.AddComponent<MeleeAttack>() as MeleeAttack;
		rbWeapon = new RangedBulletAttack ();//gameObject.AddComponent<RangedBulletWeapon>() as
					//									RangedBulletWeapon;

		currentAttack = attackTypes.Melee;
	}

	public void setAttackType (attackTypes attackType) {
		this.currentAttack = attackType;
	}

  // execute attack depending on the player attack type
	public void Attack () {
		switch (currentAttack) {
			case attackTypes.Melee:
				this.meleeAttack.Attack();
				break;
			case attackTypes.RangedBullet:
				break;
			case attackTypes.RangedRaycast:
				break;
		}
		//animate ();
	}


	public void animate () {
		this.GetComponentInChildren<Animator> ().speed = 4;
		this.GetComponentInChildren<Animator>().SetTrigger("Swung");
	}

 /*
	#region Melee Attack Scanner

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player")
		{
			if (col.gameObject.GetComponent<NetworkPlayer> ().pTeam != this.GetComponent<NetworkPlayer> ().pTeam)
			{
				meleeAttack.nearEnemy.Add (col.gameObject);
			}
		}
		//Debug.Log ("Enemy in Range");
	}


	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "Player")
		{
			if (col.gameObject.GetComponent<NetworkPlayer> ().pTeam != this.GetComponent<NetworkPlayer> ().pTeam)
			{
				meleeAttack.nearEnemy.Remove (col.gameObject);
			}
		}
		//Debug.Log ("Enemy left Range");
	}
	#endregion*/
}
