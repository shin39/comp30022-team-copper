﻿using UnityEngine;
using System.Collections;

public enum itemType {

	Weapon,
	Support

};

public class Item : Photon.PunBehaviour,IPunObservable {
	public bool isPicked; //{get;set;}

	// Use this for initialization
	void Start () {
		isPicked = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// parts that sync the data of this client player to others
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		// health synchronization

		if (stream.isWriting)
		{
			// We own this player: send the others our data
			stream.SendNext(this.isPicked);

		}
		else
		{
			// Network player, receive data
			this.isPicked = (bool)stream.ReceiveNext();

		}
	}

}
