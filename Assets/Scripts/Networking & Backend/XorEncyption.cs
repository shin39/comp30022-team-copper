﻿using UnityEngine;
using System.Collections;
using System;

public class XorEncyption : MonoBehaviour, IEncryption {

    private const int magicNumber = 555;

    // use xor cipher block chaining algorithm
    public string Encrypt(string password)
    {
        int key = magicNumber;
        string encrypted = "";

        foreach(char c in password)
        {
            encrypted += Convert.ToChar(c ^ key);
            key = c ^ key;
        }
        return encrypted;
    }
}
