﻿using UnityEngine;
using System.Collections;

public interface IEncryption {

    string Encrypt(string password);
}
