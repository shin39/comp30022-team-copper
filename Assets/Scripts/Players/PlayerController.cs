﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


// Script responsible for managing player movement and action command
public class PlayerController : MonoBehaviour {

  public const int SPEED_DIVISOR = 5;

  NetworkPlayer nPlayer;

  UnityEngine.Events.UnityAction castSk1;
  UnityEngine.Events.UnityAction castSk2;
  UnityEngine.Events.UnityAction useItem;
  UnityEngine.Events.UnityAction pickUp;

  GameObject bSkill1;
  GameObject bSkill2;
  GameObject bItem;
  GameObject bPUpItem;
  //GameObject bDropItem;

  // enumerator for action type
  public enum actionType : int
  {

		Basic       = 1,
		FirstSkill  = 2,
		SecondSkill = 3,
		Item        = 4,
		PickUpitem  = 5

	};

  void Start()
  {
    nPlayer = GetComponent<NetworkPlayer>();
    //UnityEngine.Events.UnityAction action = () => { myMethod(); myOtherMethod(); };
		this.castSk1 = () => {if (nPlayer.photonView.isMine)
                            {
				                      nPlayer.RouteInput(actionType.FirstSkill);
                            }
		                      };
    this.castSk2 = () => {if (nPlayer.photonView.isMine)
                            {
			                        nPlayer.RouteInput(actionType.SecondSkill);
                            }
                         };
    this.useItem = () => {if (nPlayer.photonView.isMine)
                            {
                              nPlayer.RouteInput(actionType.Item);
                            }
                         };
    this.pickUp  = () => {if (nPlayer.photonView.isMine)
                            {
                              nPlayer.RouteInput(actionType.PickUpitem);
                            }
                         };

    //ButtonObj = GameObject.Find("Button").GetComponent
    bSkill1 = GameObject.Find("Skill1");
    bSkill2 = GameObject.Find("Skill2");
    bItem   = GameObject.Find("Attack");
    bPUpItem = GameObject.Find("Pickup");
    //bDropItem = GameObject.Find("Drop");

    bSkill1.GetComponent<Button>().onClick.AddListener(castSk1);
    bSkill2.GetComponent<Button>().onClick.AddListener(castSk2);
    bItem.GetComponent<Button>().onClick.AddListener(useItem);
    bPUpItem.GetComponent<Button>().onClick.AddListener(pickUp);
    //bDropItem.GetComponent<Button>().onClick.AddListener(pickUp);

    //myButton.Getcomponent<Button>().onClick.AddListener(action);

    //myButton.Getcomponent().onClick.RemoveListener(action);
  }

  public actionType CastButton1()
  {
    return actionType.FirstSkill;
  }

  public actionType CastButton2()
  {
    return actionType.SecondSkill;
  }

  // move the player based on the speed and the input
	public void Move(float horz, float vert, float speed)
  	{
    	Vector3 xMove = transform.right * horz;
    	Vector3 yMove = transform.forward * vert;

    	Vector3 velocity = (xMove + yMove).normalized * speed / SPEED_DIVISOR;

    	gameObject.transform.localPosition += velocity;

	}

	public void TouchMove(float horz, float vert, float speed)
	{
		Vector3 xMove = transform.right * horz;
		Vector3 yMove = transform.forward * vert;

		Vector3 velocity = (xMove + yMove).normalized * speed / SPEED_DIVISOR;

		gameObject.transform.localPosition += velocity;
	}

	public void TouchLook(float horz, float vert, float speed)
	{
		/*
		if (CrossPlatformInputManager.GetAxis("RVertical") != 0)
		{
			float zAngle = CrossPlatformInputManager.GetAxis("RHorizontal") / CrossPlatformInputManager.GetAxis("RVertical");
			float degree = Mathf.Rad2Deg * Mathf.Atan (zAngle);
			this.transform.rotation = Quaternion.Euler (Vector3.up * degree);
		}
		*/
		if (vert != 0)
		{
			float zAngle = horz / Mathf.Abs(vert);
			float degree = Mathf.Rad2Deg * Mathf.Atan (zAngle);
			degree *= Time.fixedDeltaTime * speed;

			//gameObject.transform.rotation = Quaternion.Euler (Vector3.up * degree);

			//gameObject.transform.Rotate
			transform.Rotate (new Vector3 (0, degree, 0), Space.Self);
		}
	}


  // event listener for keypress event that leads to player actions
  // other than movements
	public actionType actionKeyListener () {

    // command for hurting self
    // for testing purpose
		if (Input.GetKeyDown(KeyCode.Z))
		{
			return actionType.Basic;
		}

    // keypress for casting first skill
		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			return actionType.FirstSkill;
		}

    // keypress for casting second skill
		if (Input.GetKeyDown(KeyCode.LeftAlt) || Input.GetKeyDown(KeyCode.LeftCommand))
		{
			return actionType.SecondSkill;
		}

    // keypress for using item
		if (Input.GetKeyDown(KeyCode.LeftControl))
		{
			return actionType.Item;
		}

    // keypress for picking up item
		if (Input.GetKeyDown(KeyCode.Space)) {
			return actionType.PickUpitem;
		}

		return 0;
	}



}
