﻿using UnityEngine;
using System.Collections;

public class CharacterDecorator : MonoBehaviour {


	public static void assignFirstSkill (ActiveSkill skill, NetworkPlayer player) {

		//player.skillHandler.firstSkill = skill;
		player.skillHandler.InitializeSkill(SkillHandler.SkillOrd.first, skill);
	}

	public static void assignSecondSkill (ActiveSkill skill, NetworkPlayer player) {

		//player.skillHandler.secondSkill = skill;
		player.skillHandler.InitializeSkill(SkillHandler.SkillOrd.second, skill);

	}

	public static void assignPassiveSkill (PassiveSkill skill, NetworkPlayer player) {

		//player.skillHandler.passive = skill;
		player.skillHandler.InitializeSkill(SkillHandler.SkillOrd.passive, skill);

	}

	public static void assignItem (Item item, NetworkPlayer player) {

		player.itemHandler.currentItem = item;

	}

}
