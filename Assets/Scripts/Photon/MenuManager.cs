﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuManager : Photon.MonoBehaviour {

    public GameObject startCanvas;
    public GameObject UICanvas;
    public Text statusText;
    public GameObject startButton;
    public GameObject panel;
	public GameObject pickup;
	public GameObject drop;

    void Start()
    {
        if (PhotonNetwork.isMasterClient)
        {
            // only allow user to start if master server
            startButton.SetActive(true);
        }
    }

    public void DisplayChat()
    {
        gameObject.GetComponent<PhotonChat>().IsVisible = !gameObject.GetComponent<PhotonChat>().IsVisible;
    }

    public void DisplayConfirmation()
    {
        Debug.Log("DisplayConfirm");
        panel.SetActive(!panel.activeSelf);
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

	public void DisplayPickup()
	{
        /*
		drop.SetActive (!drop.activeSelf);
		pickup.SetActive (!pickup.activeSelf);
        */
	}

    
    public void StartGame()
    {
        if(PhotonNetwork.room.playerCount > 1)
        {
            gameObject.GetComponent<GameManager>().BeginRound();
            this.photonView.RPC("CanvasSwitch", PhotonTargets.All);
        }      
    }

    [PunRPC]
    public void CanvasSwitch()
    {
        startCanvas.SetActive(false);
        UICanvas.SetActive(true);
    }

    public void SetStatusText(int playerConnected, int playerMax)
    {
        string str = "(" + playerConnected + " / " + playerMax + ") CONNECTED";
        statusText.text = str;
    }


}
