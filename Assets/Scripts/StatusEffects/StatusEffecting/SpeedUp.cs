﻿using UnityEngine;
using System.Collections;

public class SpeedUp : StatusEffect, IStatusEffect {

	// Use this for initialization

	private float speedUpFactor;

	public SpeedUp (float speedUpFactor, float duration) {

		this.speedUpFactor = speedUpFactor;
		this.duration = 2f;
		this.type = statusEffectType.Buff;
	}

	override public void Apply (NetworkPlayer player) {
		player.attributeHandler.speedModifier *= speedUpFactor;
		//player.playerController.forwardSpeed *= speedUpFactor;
	}

	override public void Remove (NetworkPlayer player) {
		player.attributeHandler.speedModifier /= speedUpFactor;
		//player.playerController.forwardSpeed /= speedUpFactor;
	}

}
