﻿using UnityEngine;
using System.Collections;

public abstract class PassiveSkill : IPassiveSkills {


	public enum passiveSkillType {
		Damaging,
		Supporting,
		Utility
	};

	protected passiveSkillType type { get; set; }
	public int level { get; set; }

}
