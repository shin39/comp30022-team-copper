﻿using UnityEngine;
using System.Collections;

public class Shield : ActiveSkill, IActiveSkills {

	#region Variables
	public const long skillID = 499583724L;

	private const float skillCoolDown = 10f;
	private const float baseShieldDefenceRate = 2f;
	private const float baseShieldDuration = 4f;
	#endregion

	public Shield (int level) : base () {

		this.level = level;
		this.coolDown = skillCoolDown;
		this.type = activeSkillType.Defensive;

	}

	override public void Activate (NetworkPlayer player) {

		player.ApplyStatus (new DefUp (baseShieldDefenceRate + (level - 1), baseShieldDuration + (level)));
		player.ApplyStatus (new ShieldEffect (baseShieldDuration + (level)));

	}

	override public GameObject TargetEnemy (Vector3 position) {
		return null;
	}
}
