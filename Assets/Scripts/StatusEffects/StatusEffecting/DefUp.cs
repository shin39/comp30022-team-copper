using UnityEngine;
using System.Collections;

/// <summary>
/// Defense up status effect
/// </summary>
public class DefUp : StatusEffect, IStatusEffect
{
  // defense up rate
  private float defUpRate;

  // takes defense up rate and duration
  public DefUp(float defRate, float duration)
  {
    this.defUpRate = defRate;
	this.duration  = duration;
    this.type      = statusEffectType.Buff;
  }

  // apply the status effect
  override public void Apply (NetworkPlayer player)
  {
    player.attributeHandler.defenceModifier *= defUpRate;
  }

  // remove the status effect
  override public void Remove (NetworkPlayer player)
  {
    player.attributeHandler.defenceModifier /= defUpRate;
  }
}
