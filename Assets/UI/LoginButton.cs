﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class LoginButton : MonoBehaviour {

    public InputField username;
    public InputField password;
    private string webData;
    private string[] webStrings;
    //InputField.SubmitEvent se;
    //public Text output;


	// Use this for initialization
    /*
	void Start () {
        se = new InputField.SubmitEvent();
        se.AddListener(SubmitInput);
        // what event to call once submitted
	}
    */

    public void SubmitInput()
    {
        string usernameText = "username: " + username.text + "\n";
        string passText = "password: " + password.text + "\n";
        //output.text = usernameText + passText;
        GetUser(username.text, password.text);
        // reactive the field
        username.text = "";
        username.ActivateInputField();
        password.text = "";
        password.ActivateInputField();
    }

    public void GetUser(string user, string pass)
    {
        Debug.Log("start of finding player in database");
        WWWForm www = new WWWForm();
        www.AddField("username", user);
        www.AddField("password", pass);
        WWW w = new WWW("http://localhost/gladiator/getuser.php", www);
        StartCoroutine(FindUser(w));
        Debug.Log("end of finding player in database");
    }

    // wait for the web to yield result from db search
    IEnumerator FindUser(WWW w)
    {
        yield return w;

        if(w.error == null)
        {
            Debug.Log("YESS");
            webData = w.text;
            webStrings = webData.Split(';');
            print("test:" + webStrings[0]);
            if(webStrings[0] == "true")
            {
                changeScence();
            }

        }
        else
        {
            Debug.Log("Error: " + w.error.ToString());
        }

    }

    private void changeScence()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene("Lobby");
    }
}


