﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Blink.
/// </summary>
public class Blink : ActiveSkill, IActiveSkills {

	#region Variables
	public const long skillID = 857394756L;

	private const float skillCoolDown = 4f;
	private const int baseDistance = 10;
	#endregion
	#region Public Methods

	public Blink (int level) : base() {
		this.level = level;
		this.coolDown = skillCoolDown;
		this.type = activeSkillType.Utility;
	}


	/// <summary>
	/// Activate the skill Blink. This teleports the player to a position in front.
	///  The player is the target.
	/// </summary>
	/// <param name="target">Target.</param>
	override public void Activate (NetworkPlayer player) {
		Transform target = player.transform;
		Vector3 newPos;
		//target.localPosition += baseDistance * Vector3.forward * level;
		target.Translate ((baseDistance * Vector3.forward * level));
		newPos = new Vector3(target.position.x, (target.position.y >= -5f && target.position.y <= 1f) ? 0 :
			target.position.y , target.position.z);
		target.position = newPos;

		//SkillHandler.startCoolDown (this);
	}


	/// <summary>
	/// Targets the enemy. Takes in the position of the character.
	/// </summary>
	/// <returns>The enemy.</returns>
	/// <param name="position">Position.</param>
	override public GameObject TargetEnemy (Vector3 position) {
		return null;
	}

	#endregion
}
