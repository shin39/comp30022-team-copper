﻿using UnityEngine;
using System.Collections;

public class SwapBullet : Bullet
{

	public NetworkPlayer caster { get; set; }


	void Start() {
		Destroy (this.gameObject, 2.0f);
	}

	void Update () {


	}

	public void setSpeed (Transform transform_, float speed) {
		GetComponent<Rigidbody>().velocity += (transform_.forward * speed);


	}

	void OnTriggerEnter(Collider collider) {
		if(collider.gameObject.tag != "Enemy")
		{
          return;
        }
		caster.ApplyStatus(new SpeedUp(2f, 0f));
		collider.GetComponent<NetworkPlayer> ().ApplyStatus (new SpeedDown (1.5f, 0f));
       /// Transform target = collider.transform;
		Swap.SwapPlayers(caster.transform, collider.transform);
		GameObject.Destroy(this.gameObject);

	}

}
