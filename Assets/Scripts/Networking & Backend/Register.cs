﻿/*
 * This class reads user input to username and password field and 
 * attempt to register the user into our database
 * written by : Erwin Haryantho <664480>
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class Register : MonoBehaviour {
    public InputField username;
    public InputField password;
    public InputField confirmPass;
    public InputField email;

    public Text status;

    // values retrieved from web
    private string webData;
    private string[] webStrings;

    // encryption algorithm
    private XorEncyption encryption;

    void Start()
    {
        encryption = new XorEncyption();
    }

    public void SubmitInput()
    {
        // check for any blank input field
        if(username.text == "" || password.text == "" || confirmPass.text == "" || email.text == "")
        {
            status.text = "can not leave one of the field blank";
            return;
        }

        // check input for non-ascii char
        if (!CheckInput(username.text))
        {
            status.text = "username may only contain alphabet and number";
            ReactivatePass();
            username.text = "";
            username.ActivateInputField();
            return;
        }

        if (!CheckInput(password.text))
        {
            status.text = "password may only contain alphabet and number";
            ReactivatePass();
            return;
        }

        // check password confirmation
        if(password.text != confirmPass.text)
        {
            status.text = "please re-enter your password";
            ReactivatePass();
            return;
        }

        // check email validity
        if (!CheckEmail(email.text))
        {
            status.text = "please enter a valid email";
            ReactivatePass();
            email.text = "";
            email.ActivateInputField();
            return;
        }
        // load loading screen
        SceneManage.DisplayLoading();
        AddUser(username.text, password.text, email.text);

        // reactive input field
        username.text = "";
        username.ActivateInputField();
        email.text = "";
        email.ActivateInputField();
        ReactivatePass();
    }

    public void AddUser(string user, string pass, string email)
    {
        string encryptedPass = encryption.Encrypt(pass);
        WWWForm www = new WWWForm();
        www.AddField("username", user);
        www.AddField("password", pass);
        www.AddField("email", email);
        WWW w = new WWW("http://115.146.87.244/adduser.php", www);
        StartCoroutine(InsertUser(w));
    }

    // wait for the web to yield result from db search
    IEnumerator InsertUser(WWW w)
    {
        yield return w;

        // web successfully returned something
        if (w.error == null)
        {
            webData = w.text;
            webStrings = webData.Split(';');
            print("test:" + webStrings[0]);
            if (webStrings[0] == "true")
            {
                status.text = "your account has been successfully registered";

                // bring back to login menu
                SceneManage.DisplayLogin();
            }
            else
            {
                // display error
                // load register form again
                SceneManage.DisplayRegister();
                status.text = webStrings[1];             
            }

        }

        // error in connecting to web
        else
        {
            status.text = w.error.ToString();
            SceneManage.DisplayRegister();
        }

    }

    private bool CheckInput(string str)
    {
        Regex r = new Regex("^[a-zA-Z0-9]*$");
        if (r.IsMatch(str))
        {
            return true;
        }
        return false;

    }

    private bool CheckEmail(string email)
    {
        Regex r = new Regex("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+[.][A-Za-z]{2,4}");
        if (r.IsMatch(email))
        {
            return true;
        }
        return false;
    }

    private void ReactivatePass()
    {
        password.text = "";
        password.ActivateInputField();
        confirmPass.text = "";
        confirmPass.ActivateInputField();
    }

    public void DisplayLogin()
    {
        SceneManage.DisplayLogin();
    }
    
}
