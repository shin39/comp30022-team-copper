﻿/*
 * This class control the scene management
 * and forms visibility
 * written by : Erwin Haryantho <664480>
 */

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneManage : MonoBehaviour {

    public static void DisplayRegister()
    {
        GameObject login = GameObject.Find("Canvas (NetworkUI)").transform.GetChild(0).gameObject;
        GameObject loading = GameObject.Find("Canvas (NetworkUI)").transform.GetChild(1).gameObject;
        GameObject register = GameObject.Find("Canvas (NetworkUI)").transform.GetChild(2).gameObject;

        login.SetActive(false);
        loading.SetActive(false);
        register.SetActive(true);
    }

    public static void DisplayLogin()
    {
        GameObject login = GameObject.Find("Canvas (NetworkUI)").transform.GetChild(0).gameObject;
        GameObject loading = GameObject.Find("Canvas (NetworkUI)").transform.GetChild(1).gameObject;
        GameObject register = GameObject.Find("Canvas (NetworkUI)").transform.GetChild(2).gameObject;

        login.SetActive(true);
        loading.SetActive(false);
        register.SetActive(false);
    }

    public static void DisplayLoading()
    {
        GameObject login = GameObject.Find("Canvas (NetworkUI)").transform.GetChild(0).gameObject;
        GameObject loading = GameObject.Find("Canvas (NetworkUI)").transform.GetChild(1).gameObject;
        GameObject register = GameObject.Find("Canvas (NetworkUI)").transform.GetChild(2).gameObject;

        login.SetActive(false);
        loading.SetActive(true);
        register.SetActive(false);
    }

    public static void ChangeScene()
    {     
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene("Lobby");   
    }

}
